package firstproject;

import java.util.*;

import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;


public  class First_Project {
	
	static WebDriver driver;
	static int oldrowsize;
	public static void main(String[] args) {
		
		System.setProperty("webdriver.firefox.marionette","geckodriver.exe");
		
		driver = new FirefoxDriver();
		
		driver.get("http://localhost:8888/c2t-WebApp1/users");
		
		
		First_Project.GeTtableRows();
		First_Project.GetTableCols();
		First_Project.GetButtonColor();
		First_Project.GetButtonProperty();
		First_Project.addUserUnsuccess();
		First_Project.LableCheck();
		First_Project.addUserSuccess();
		First_Project.Record_check();
	}
	
	public static void GeTtableRows(){
		
		
		List<WebElement>row= driver.findElements(By.xpath("//table[@class='table table-striped']/tbody/tr"));
		oldrowsize = row.size();
		System.out.println(""+oldrowsize);
		
		
	}
	
	public static void GetTableCols(){
			List<WebElement>col = driver.findElements(By.xpath("//table[@class='table table-striped']/thead/tr/th"));
		
			System.out.println(""+col.size());
	}
	
	public static void GetButtonColor(){
		
		String buttoncolor=driver.findElement(By.xpath("//table[@class='table table-striped']/tbody/tr[1]/td[5]/button[1]")).getCssValue("background-color");
	
		System.out.println(""+buttoncolor);
	}
	public static void GetButtonProperty(){
		
		Boolean Enable = driver.findElement(By.xpath("//table[@class='table table-striped']/tbody/tr[1]/td[5]/button[2]")).isEnabled();
		
		if (Enable==true) {
			System.out.println( "button enabled");
		
		}
		 else {
			 System.out.println("button disbaled");
		}
	}
	
	
	public static void 	LableCheck(){
		
		driver.findElement(By.xpath("//a[text()='Add User']")).click();
				
		String Name=driver.findElement(By.xpath(".//*[@id='userForm']/div[1]/label")).getText();
		
		if (Name.equalsIgnoreCase("Name")) {
			System.out.println("Lable Correct");
		} else {
			System.out.println("Lable Incorrect");
		}
		
	}
	
	public static void addUserUnsuccess(){
		
	driver.findElement(By.xpath("//a[text()='Add User']")).click();
	
	driver.findElement(By.xpath(".//*[@id='name']")).clear();
	
	driver.findElement(By.xpath(".//*[@id='userForm']/div[12]/div/button")).click();
	
	if (driver.findElement(By.xpath(".//*[@id='name.errors']")).isDisplayed()==true) {
		System.out.println("Error Present");
	} else {
		System.out.println("Error not present");
	}
		
	}
	public static void addUserSuccess(){
		
	//	driver.findElement(By.xpath("//a[text()='Add User']")).click();
		
		driver.findElement(By.xpath(".//*[@id='name']")).clear();
		driver.findElement(By.xpath(".//*[@id='name']")).sendKeys("testuser03");
		driver.findElement(By.xpath(".//*[@id='email']")).clear();
		driver.findElement(By.xpath(".//*[@id='email']")).sendKeys("test@test.com");
		driver.findElement(By.xpath(".//*[@name='password']")).sendKeys("password@1234");
		driver.findElement(By.xpath(".//*[@name='confirmPassword']")).sendKeys("password@1234");
		driver.findElement(By.xpath(".//*[@id='address']")).clear();
		driver.findElement(By.xpath(".//*[@id='address']")).sendKeys("Infosys,Pune");
		
		driver.findElement(By.xpath(".//*[@id='newsletter']")).click();
		driver.findElement(By.xpath(".//*[@id='framework1']")).click();
		driver.findElement(By.xpath(".//*[@id='framework2']")).click();
		
		Select country =new Select(driver.findElement(By.xpath(".//*[@id='country']")));
		country.selectByVisibleText("China");
		
		Select skill =new Select(driver.findElement(By.xpath(".//*[@id='skill']")));
		if (skill.isMultiple()==true) {
			skill.deselectAll();
			skill.selectByValue("Hibernate");
			skill.selectByIndex(2);
			skill.selectByVisibleText("Grails");
		}

		driver.findElement(By.xpath(".//*[@id='userForm']/div[12]/div/button")).click();
		driver.findElement(By.xpath("//a[@class='navbar-brand']")).click();
		}
	
	public static void Record_check(){
			
		List<WebElement>newrow= driver.findElements(By.xpath("//table[@class='table table-striped']/tbody/tr"));
		int newrowsize = newrow.size();
	
		if (newrowsize==(oldrowsize+1)) {
			
			System.out.println("RECORD added");
		} else {
			System.out.println("Record not addded");
		}
	}
}























































